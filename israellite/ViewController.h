//
//  ViewController.h
//  israellite
//
//  Created by Jim Snavely on 8/30/14.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ViewController : UIViewController <UIWebViewDelegate>

@property IBOutlet UIWebView *webView;

@end
