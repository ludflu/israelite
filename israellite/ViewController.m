//
//  ViewController.m
//  israellite
//
//  Created by Jim Snavely on 8/30/14.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString* url = @"https://s3.amazonaws.com/israelite/index.html";
    NSURL* nsUrl = [NSURL URLWithString:url];
    NSURLRequest *req = [NSURLRequest requestWithURL:nsUrl];
    NSLog(@"were in viewdidload");
    [self.webView loadRequest:req];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"were in webViewDidFinishLoad");
    
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    NSLog(@"were in webViewDidStartLoad");
    
}

@end
